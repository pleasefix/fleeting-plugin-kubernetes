package kubernetes

import (
	"context"
	"fmt"
	"path"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

type InstanceGroup struct {
}

func (g *InstanceGroup) Init(ctx context.Context, log hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	return provider.ProviderInfo{
		ID:        path.Join("kubernetes"),
		MaxSize:   1000,
		Version:   Version.String(),
		BuildInfo: Version.BuildInfo(),
	}, nil
}

func (g *InstanceGroup) Update(ctx context.Context, update func(id string, state provider.State)) error {
	return fmt.Errorf("not implemented")
}

func (g *InstanceGroup) Increase(ctx context.Context, delta int) (int, error) {
	return 0, fmt.Errorf("not implemented")
}

func (g *InstanceGroup) Decrease(ctx context.Context, instances []string) ([]string, error) {
	return nil, fmt.Errorf("not implemented")
}

func (g *InstanceGroup) ConnectInfo(ctx context.Context, id string) (provider.ConnectInfo, error) {
	return provider.ConnectInfo{}, fmt.Errorf("not implemented")
}
