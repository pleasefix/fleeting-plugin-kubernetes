package main

import (
	"flag"
	"fmt"
	"os"

	kubernetes "gitlab.com/gitlab-org/fleeting/fleeting-plugin-kubernetes"
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
)

var (
	showVersion = flag.Bool("version", false, "Show version information and exit")
)

func main() {
	flag.Parse()
	if *showVersion {
		fmt.Println(kubernetes.Version.Full())
		os.Exit(0)
	}

	plugin.Serve(&kubernetes.InstanceGroup{})
}
