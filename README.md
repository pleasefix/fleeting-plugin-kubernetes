# Fleeting Plugin Kubernetes

This is a [fleeting](https://gitlab.com/gitlab-org/fleeting/fleeting) plugin for Kubernetes.

[![Pipeline Status](https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws/badges/main/pipeline.svg)](https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws)](https://goreportcard.com/report/gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws)

## Building the plugin

To generate the binary, ensure `$GOPATH/bin` is on your PATH, then use `go install`:

```shell
cd cmd/fleeting-plugin-kubernetes/
go install 
```

If you are managing go versions with asdf, run this after generating the binary:

```shell
asdf reshim
```
